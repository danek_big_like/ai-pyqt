import sys
import json
import requests
from PIL import Image
from io import BytesIO
import base64
from PyQt5.QtCore import Qt, QPoint, pyqtSignal
from PyQt5.QtGui import QPixmap, QPainter, QPen, QColor
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QAction, QFileDialog, QToolBar, QColorDialog, QSlider, \
    QLineEdit, QPushButton, QVBoxLayout, QDialog, QComboBox


class SettingsEditor(QDialog):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Edit Generation Settings")
        self.setGeometry(100, 100, 400, 200)

        self.modelLabel = QLabel("SD Model:")
        self.modelInput = QComboBox(self)
        self.modelInput.addItems(["auto"])

        # Получаем список доступных моделей
        self.available_models = self.get_available_models()
        if self.available_models:
            self.modelInput.addItems(self.available_models)

        self.imageTypeLabel = QLabel("Image Type:")
        self.imageTypeInput = QLineEdit(self)

        self.imageTypeInput.setText("txt2img")

        self.imageWidthLabel = QLabel("Image Width:")
        self.imageWidthInput = QLineEdit(self)

        self.imageHeightLabel = QLabel("Image Height:")
        self.imageHeightInput = QLineEdit(self)

        self.imageGenStepsLabel = QLabel("Image Gen Steps:")
        self.imageGenStepsInput = QLineEdit(self)

        self.saveButton = QPushButton("Save", self)
        self.saveButton.setShortcut("Ctrl+S")
        self.saveButton.clicked.connect(self.saveSettings)

        self.errorLabel = QLabel("")
        self.errorLabel.setStyleSheet("color: red;")

        try:
            with open('settings.json', 'r') as file:
                settings = json.load(file)
                self.modelInput.setCurrentText(settings.get('model'))
                self.imageWidthInput.setText(str(settings.get('image_width')))
                self.imageHeightInput.setText(str(settings.get('image_height')))
                self.imageGenStepsInput.setText(str(settings.get('gen_steps')))
                self.imageTypeInput.setText(settings.get('image_type'))

        except FileNotFoundError:
            pass

        layout = QVBoxLayout()
        layout.addWidget(self.modelLabel)
        layout.addWidget(self.modelInput)
        layout.addWidget(self.imageTypeLabel)
        layout.addWidget(self.imageTypeInput)
        layout.addWidget(self.imageWidthLabel)
        layout.addWidget(self.imageWidthInput)
        layout.addWidget(self.imageHeightLabel)
        layout.addWidget(self.imageHeightInput)
        layout.addWidget(self.imageGenStepsLabel)
        layout.addWidget(self.imageGenStepsInput)
        layout.addWidget(self.saveButton)
        layout.addWidget(self.errorLabel)

        self.setLayout(layout)

    def get_available_models(self):
        try:
            # Выполняем GET-запрос к API для получения доступных моделей
            response = requests.get("http://127.0.0.1:7860/sdapi/v1/sd-models")
            if response.status_code == 200:
                data = response.json()
                models = [model["model_name"] for model in data]
                return models

        except requests.exceptions.RequestException as e:
            print(f"Failed to fetch available models: {str(e)}")
            return []

    def saveSettings(self):
        model = self.modelInput.currentText()
        image_type = self.imageTypeInput.text()
        image_width_str = self.imageWidthInput.text()
        image_height_str = self.imageHeightInput.text()
        gen_steps_str = self.imageGenStepsInput.text()

        # Проверяем, что введенные значения можно преобразовать в числа
        if not image_width_str.isdigit() or not image_height_str.isdigit() or not gen_steps_str.isdigit():
            self.errorLabel.setText("Please enter valid numeric values.")
            return
        if (not model in self.available_models) and model != "auto":
            self.errorLabel.setText("Model not exist")
            return

        image_width = int(image_width_str)
        image_height = int(image_height_str)
        gen_steps = int(gen_steps_str)

        # Проверяем, что размеры делятся на 8
        if image_width % 8 != 0 or image_height % 8 != 0:
            self.errorLabel.setText("Image dimensions must be multiples of 8.")
            return

        settings = {
            "model": model,
            "image_type": image_type,
            "image_width": image_width,
            "image_height": image_height,
            "gen_steps": gen_steps
        }

        with open('settings.json', 'w') as file:
            json.dump(settings, file)

        self.accept()


class ImageGenerationTool(QMainWindow):
    generated_image_signal = pyqtSignal(QPixmap)  # Сигнал для передачи сгенерированного изображения

    def __init__(self, main_window):
        super().__init__()
        self.imageType = 'txt2img'
        self.model = 'auto'
        self.imageWidth = 512
        self.imageHeight = 512
        self.imageGenSteps = 25
        self.main_window = main_window
        main_window.setImageGenTo(self)
        self.setWindowTitle("Image Generation Tool")
        self.setGeometry(100, 100, 300, 200)

        self.loadSettings()
        # Здесь вы можете добавить элементы интерфейса для вашего инструмента генерации изображений

        # Например:
        # self.generateImageButton = QPushButton("Generate Image", self)
        # self.generateImageButton.clicked.connect(self.generateImage)
        # self.setCentralWidget(self.generateImageButton)

        self.promptInput = QLineEdit(self)
        self.promptInput.setGeometry(10, 10, 200, 30)

        self.negativPromptInput = QLineEdit(self)
        self.negativPromptInput.setGeometry(10, 50, 200, 30)

        self.generateButton = QPushButton("Generate Image", self)
        self.generateButton.setGeometry(10, 90, 200, 30)
        self.generateButton.clicked.connect(self.generateImage)

        self.editSettingsButton = QPushButton("Edit Settings", self)
        self.editSettingsButton.setGeometry(10, 130, 200, 30)
        self.editSettingsButton.clicked.connect(self.openSettingsEditor)

        self.show()

    def loadSettings(self):
        try:
            with open('settings.json', 'r') as file:
                settings = json.load(file)
                # Здесь вы можете извлечь настройки из файла и применить их к вашему инструменту

                # Например:
                self.model = settings.get('model')
                self.imageWidth = settings.get('image_width')
                self.imageHeight = settings.get('image_height')
                self.imageGenSteps = settings.get('gen_steps')
                self.imageType = settings.get('image_type')

        except FileNotFoundError:
            # Если файл settings.json не существует, установите значения по умолчанию
            pass

    # Здесь вы можете добавить функциональность для генерации изображений
    # def generateImage(self):
    #     ...

    def generateImage(self):
        print(self.promptInput.text())
        print(self.negativPromptInput.text())
        url = "http://127.0.0.1:7860"
        prompt = self.promptInput.text()
        negativPrompt = self.negativPromptInput.text()
        payload = {
            "prompt": prompt,
            "negative_prompt": negativPrompt,
            "steps": int(self.imageGenSteps),
            "width": int(self.imageWidth),
            "height": int(self.imageHeight),
        }

        if self.model != 'auto':
            model = {
                "sd_model_checkpoint": self.model
            }
            setmodel = requests.post(url=f'{url}/sdapi/v1/options', json=model)

        response = requests.post(url=f'{url}/sdapi/v1/{self.imageType}', json=payload)

        if response.status_code == 200:
            r = response.json()
            encoded_image = r['images'][0]
            image_data = base64.b64decode(encoded_image)
            image = Image.open(BytesIO(image_data))

            # Создаем QPixmap из сгенерированного изображения
            generated_pixmap = QPixmap()
            generated_pixmap.loadFromData(image_data)

            # Используем сигнал для передачи сгенерированного изображения в основное окно
            self.generated_image_signal.emit(generated_pixmap)
        else:
            print(response.status_code)

    def openSettingsEditor(self):
        settings_editor = SettingsEditor()
        if settings_editor.exec_() == QDialog.Accepted:
            self.loadSettings()
            # self.promptInput.setText("")  # Сбросить поле ввода


class Window(QMainWindow):

    def __init__(self):
        super().__init__()

        self.tool = None
        self.pen_color = Qt.black  # Начальный цвет карандаша
        self.pen_size = 2  # Начальный размер карандаша

        self.setWindowTitle("Image Editor")
        self.setGeometry(100, 100, 800, 600)

        self.image = QLabel()
        self.image.setAlignment(Qt.AlignTop | Qt.AlignLeft)
        self.setCentralWidget(self.image)

        self.drawing = False
        self.lastPoint = QPoint()

        self.fileMenu = self.menuBar().addMenu("File")
        # self.toolsMenu = self.menuBar().addMenu("Tools")

        self.openAction = QAction("Open", self)
        self.openAction.triggered.connect(self.openImage)
        self.openAction.setShortcut("Ctrl+O")
        self.fileMenu.addAction(self.openAction)

        self.saveAction = QAction("Save", self)
        self.saveAction.triggered.connect(self.saveImage)
        self.saveAction.setShortcut("Ctrl+S")
        self.fileMenu.addAction(self.saveAction)

        self.clearAction = QAction("Clear Stack", self)
        self.clearAction.triggered.connect(self.clearStack)
        self.clearAction.setShortcut("Ctrl+DEL")
        self.fileMenu.addAction(self.clearAction)

        self.pixmap = QPixmap()
        self.image.setPixmap(self.pixmap)

        self.stack = []

        self.createToolbar()  # Добавляем панель инструментов

        self.toolsMenu = self.menuBar().addMenu("Tools")

        self.undo_stack = []
        self.current_index = -1  # Индекс текущей версии изображения

        # Добавляем ваш инструмент генерации изображений в меню Tools
        self.imageGenerationAction = QAction("Image Generation Tool", self)
        self.imageGenerationAction.triggered.connect(self.openImageGenerationTool)
        self.imageGenerationAction.setShortcut("Ctrl+G")
        self.toolsMenu.addAction(self.imageGenerationAction)

        self.show()

    def setImageGenTo(self, igt):
        self.igt = igt

    def createToolbar(self):
        toolbar = QToolBar()
        self.addToolBar(toolbar)

        self.penAction = QAction("Pen", self)
        self.penAction.triggered.connect(self.setPen)
        toolbar.addAction(self.penAction)

        self.eraserAction = QAction("Eraser", self)
        self.eraserAction.triggered.connect(self.setEraser)
        toolbar.addAction(self.eraserAction)

        self.selectAction = QAction("Select", self)
        self.selectAction.triggered.connect(self.setSelect)
        toolbar.addAction(self.selectAction)

        self.colorPickerAction = QAction("Color", self)
        self.colorPickerAction.triggered.connect(self.setColorPicker)
        toolbar.addAction(self.colorPickerAction)

        self.undoAction = QAction("Undo", self)
        self.undoAction.triggered.connect(self.undo)
        self.undoAction.setShortcut("Ctrl+Z")
        toolbar.addAction(self.undoAction)

        self.redoAction = QAction("Redo", self)
        self.redoAction.triggered.connect(self.redo)
        self.redoAction.setShortcut("Ctrl+Shift+Z")
        toolbar.addAction(self.redoAction)

        # Добавляем QSlider для настройки размера карандаша
        size_slider = QSlider(Qt.Horizontal)
        size_slider.setMinimum(1)
        size_slider.setMaximum(10)
        size_slider.setValue(self.pen_size)
        size_slider.valueChanged.connect(self.setPenSize)
        toolbar.addWidget(size_slider)

    def openImageGenerationTool(self):
        self.imageGenerationTool = ImageGenerationTool(self)
        self.imageGenerationTool.generated_image_signal.connect(self.updateGeneratedImage)
        self.imageGenerationTool.show()

    def openImage(self):
        filename, _ = QFileDialog.getOpenFileName(None, "Open Image", "", "Images (*.png *.jpg *.bmp *.jpeg *.jpg)")
        if filename:
            self.pixmap = QPixmap(filename)
            self.image.setPixmap(self.pixmap)
            #self.stack.clear()  # Очистите стек, так как это новое изображение
            self.saveToUndoStack()  # Сохраните текущую версию изображения в стек


    def saveImage(self):
        filename, _ = QFileDialog.getSaveFileName(None, "Save Image", "", "Images (*.png *.jpg *.bmp *.jpeg *.jpg)")
        if filename:
            self.pixmap.save(filename)

    def setPen(self):
        self.tool = 'draw'

    def setEraser(self):
        self.tool = 'erase'

    def setSelect(self):
        self.tool = 'select'

    def setColorPicker(self):
        color = QColorDialog.getColor()
        if color.isValid():
            self.pen_color = color

    def setPenSize(self, size):
        self.pen_size = size

    def saveToUndoStack(self):
        # Если мы откатились к более старой версии, удаляем версии после текущей
        if self.current_index < len(self.undo_stack) - 1:
            del self.undo_stack[self.current_index + 1:]
        # Сохраняем текущую версию изображения в стек
        self.undo_stack.append(self.pixmap.copy())
        self.current_index = len(self.undo_stack) - 1

    def clearStack(self):
        self.stack.clear()

    def undo(self):
        if self.current_index > 0:
            self.current_index -= 1
            self.pixmap = self.undo_stack[self.current_index]
            self.image.setPixmap(self.pixmap)
        print(self.current_index)

    def redo(self):
        if self.current_index < len(self.undo_stack) - 1:
            self.current_index += 1
            self.pixmap = self.undo_stack[self.current_index]
            self.image.setPixmap(self.pixmap)
        print(self.current_index)

    def mousePressEvent(self, event):
        if self.tool == 'draw':
            self.lastPoint = self.image.mapFromGlobal(event.globalPos())
        if self.tool == 'select':
            self.lastPoint = self.image.mapFromGlobal(event.globalPos())

    def mouseMoveEvent(self, event):
        if self.tool == 'draw':
            currentPos = self.image.mapFromGlobal(event.globalPos())
            painter = QPainter(self.pixmap)
            painter.setPen(QPen(self.pen_color, self.pen_size))
            painter.drawLine(self.lastPoint, currentPos)
            self.lastPoint = currentPos
            self.image.setPixmap(self.pixmap)
        if self.tool == 'select':
            currentPos = self.image.mapFromGlobal(event.globalPos())
            r = self.lastPoint.x(), self.lastPoint.y(), currentPos.x() - self.lastPoint.x(), currentPos.y() - self.lastPoint.y()
            self.image.setPixmap(self.pixmap.copy(*r))
            self.stack.append(self.pixmap.copy(*r))

    def mouseReleaseEvent(self, event):
        if self.tool == 'draw':
            currentPos = self.image.mapFromGlobal(event.globalPos())
            painter = QPainter(self.pixmap)
            painter.setPen(QPen(self.pen_color, self.pen_size))
            painter.drawLine(self.lastPoint, currentPos)
            self.lastPoint = currentPos
            self.image.setPixmap(self.pixmap)
            self.stack.append(self.pixmap.copy())  # Сохраняем копию pixmap вместо ссылки
            self.saveToUndoStack()  # Сохраняем текущую версию изображения в стек
        elif self.tool == 'select':
            currentPos = self.image.mapFromGlobal(event.globalPos())
            r = self.lastPoint.x(), self.lastPoint.y(), currentPos.x() - self.lastPoint.x(), currentPos.y() - self.lastPoint.y()
            self.image.setPixmap(self.pixmap.copy(*r))
            self.stack.append(self.pixmap.copy(*r))
            self.saveToUndoStack()  # Сохраняем текущую версию изображения в стек

    def updateGeneratedImage(self, pixmap):
        # Обновляем изображение в основном окне с использованием сгенерированного изображения
        self.pixmap = pixmap
        self.image.setPixmap(self.pixmap)
        #self.stack.clear()  # Очистите стек, так как это новое изображение
        self.saveToUndoStack()  # Сохраните текущую версию изображения в стек



app = QApplication(sys.argv)
window = Window()
# image_generation_tool = ImageGenerationTool(window)
# image_generation_tool.generated_image_signal.connect(window.updateGeneratedImage)  # Подключаем сигнал к слоту
app.exec()
